<?php

/**
 * Handler database
 */
class database
{
    // credentials
    private $host = "127.0.0.1";
    private $user = "andrealmeida";         
    private $pass = "";                     
    private $db = "dbtest";               
    private $port = 3306;
    
    public $conn;
    
    /**
     * Handler database connection
     */
    public function getConnection()
    {
        $this->conn = null;
        
        try
        {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db, $this->user, $this->pass);
            $this->conn->exec("set names utf8");
            
            
        } catch (PDOException $exception)
        {
            echo "Error: " . $exception->getMessage();
        }
        
        return $this->conn;
    }
}

?>