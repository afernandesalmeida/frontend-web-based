<?php

define('status','status');
define('statusOK','200');
define('statusFAIL','500');
define('statusNODATA','400');
define('message','message');
define('messageNODATA','No data.');
define('messageOkCreate','Person was successfully created.');
define('messageFailCreate','Unable to create person.');
define('messageOkRead','Finded.');
define('messageFailRead','No person has been found.');
define('messageOkUpdate','The person was successfully updated.');
define('messageFailUpdate','Error updating person.');
define('messageOkDelete','The person was successfully deleted.');
define('messageFailDelete','Error when trying deleting person.');

?>