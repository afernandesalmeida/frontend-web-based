<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include strings
include_once '../strings/strings.php';
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/people.php';
 
// instantiate database and pepple object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$people = new people($db);
 
// query people
$stmt = $people->readByAge();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){
 
    $age=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

        $age = $row;
 
    }
 
    echo json_encode($age);
}
 
else{
    echo json_encode(array(status => statusNODATA, message => messageNODATA));
}
?>