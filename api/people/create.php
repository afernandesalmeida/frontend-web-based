<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include strings
include_once '../strings/strings.php';
 
// get database connection
include_once '../config/database.php';
 
// instantiate people object
include_once '../objects/people.php';
 
$database = new Database();
$db = $database->getConnection();
 
$people = new people($db);
 
 
//var_dump(file_get_contents("php://input"));
// get posted data
$data = json_decode(file_get_contents("php://input"));

//var_dump($data);

// check data
if ($data != NULL)
{
    // simple check data format
    $i = 0;
    foreach ($data as $key => $value)
    {
        
        if($key == "name" && $value != NULL)
        {
            $i=$i+1;
            
        }elseif ($key == "birthday" && $value != NULL) 
        {
            $i=$i+1;
            
        }elseif ($key == "document" && $value != NULL)
        {
            $i=$i+1;
            
        }elseif ($key == "gender" && $value != NULL)
        {
            $i=$i+1;
        }
    
        // if all needed information is OK
        if ($i == 4) 
        {
            //echo "valid format!";
            break;
        }
        
    }
    
    // die if something is wrong
    if ($i != 4) 
    {
        echo json_encode(array(status => statusNODATA, message => messageNODATA));
        die();
    }
    
    // if good format, insert
    
    // set people values
    $people->name = $data->name;
    $people->birthday = $data->birthday;
    $people->document = $data->document;
    $people->gender = $data->gender;
    $people->address = $data->address;
     
    // insert people
    if($people->insert())
    {
        echo json_encode(array(status => statusOK, message => messageOkCreate));
    }
 
    // if something wrong happens, warn
    else
    {
        echo json_encode(array(status => statusFAIL, message => messageFailCreate));
    }
} else

{
    echo json_encode(array(status => statusNODATA, message => messageNODATA));
    
}



?>