<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include strings
include_once '../strings/strings.php';
 
// get database connection
include_once '../config/database.php';
 
// instantiate people object
include_once '../objects/people.php';
 
$database = new Database();
$db = $database->getConnection();
 
$people = new people($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// check data
if ($data != NULL)
{
    // check if id is present
    $i = 0;
    foreach ($data as $key => $value)
    {
        
        if($key == "id" && $value != NULL)
        {
            $i=$i+1;
            break;
            
        }
        
    }
    
    // die if id not found
    if ($i != 1) 
    {
        echo json_encode(array(status => statusNODATA, message => messageNODATA));
        die();
    }
    
    // if good format, insert
    
    // set people values
    $people->id = $data->id;
    $people->name = $data->name;
    $people->birthday = $data->birthday;
    $people->document = $data->document;
    $people->gender = $data->gender;
    $people->address = $data->address;
     
    // insert people
    if($people->update())
    {
        echo json_encode(array(status => statusOK, message => messageOkUpdate));
    }
 
    // if something wrong happens, warn
    else
    {
        echo json_encode(array(status => statusFAIL, message => messageFailUpdate));
    }
} else

{
    echo json_encode(array(status => statusNODATA, message => messageNODATA));
    
}



?>