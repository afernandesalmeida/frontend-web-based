<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include strings
include_once '../strings/strings.php';

// get database connection
include_once '../config/database.php';
 
// instantiate people object
include_once '../objects/people.php';
 
$database = new Database();
$db = $database->getConnection();
 
$people = new people($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// check data
if ($data != NULL)
{
    // check if id is present
    $i = 0;
    foreach ($data as $key => $value)
    {
        
        if($key == "id" && $value != NULL)
        {
            $i=$i+1;
            break;
            
        }
        
    }
    
    // die if id not found
    if ($i != 1) 
    {
        //echo "invalid format!";
        die();
    }
    
    // if good format, insert
    
    // set id people values
    $people->id = $data->id;
     
    // delete people
    if($people->delete())
    {
        echo json_encode(array(status => statusOK, message => messageOkDelete));
    }
 
    // if something wrong happens, warn
    else
    {
        echo json_encode(array(status => statusFAIL, message => messageFailDelete));
    }
} else

{
    echo json_encode(array(status => statusNODATA, message => messageNODATA));
    
}



?>