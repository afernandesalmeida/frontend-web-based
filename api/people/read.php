<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include strings
include_once '../strings/strings.php';
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/people.php';
 
// instantiate database and pepple object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$people = new people($db);
 
// query people
$stmt = $people->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // peoples array
    $people_arr=array();
    $people_arr["results"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $people_item=array(
            "id" => $id,
            "name" => $name,
            "birthday" => $birthday,
            "document" => $document,
            "gender" => $gender,
            "address" => $address
        );
 
        array_push($people_arr["results"], $people_item);
    }
 
    echo json_encode($people_arr);
}
 
else{
    echo json_encode(array(status => statusNODATA, message => messageNODATA));
}
?>