<?php

/**
 * People object
 */
class people
{
    
    private $conn;
    private $tbl_name = "people";
    
    // object attributes
    
    public $id;
    public $name;
    public $birthday;
    public $document;
    public $gender;
    public $address;
    
    /**
     * constructor using $db as database connection
     */
    public function __construct($db)
    {
        $this->conn = $db;
    }
    
    // read people
    function read()
    {
     
        // select all query
        $query = "SELECT
                    p.id
                    ,p.name
                    ,p.birthday
                    ,p.document
                    ,p.gender
                    ,p.address
                FROM
                    " . $this->tbl_name . " p
                ORDER BY
                    p.id";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
    // insert people
    function insert()
    {
     
        // query to insert record
        $query = "INSERT INTO
                    " . $this->tbl_name . "
                  SET
                    name=:name
                    ,birthday=:birthday
                    ,document=:document
                    ,gender=:gender
                    ,address=:address";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->birthday=htmlspecialchars(strip_tags($this->birthday));
        $this->document=htmlspecialchars(strip_tags($this->document));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->address=htmlspecialchars(strip_tags($this->address));
     
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":birthday", $this->birthday);
        $stmt->bindParam(":document", $this->document);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":address", $this->address);
        
        //var_dump($stmt);
     
        // execute query
        if($stmt->execute())
        {
            
            return true;
        }
     
        return false;
         
    }
    
    
    // update people
    function update()
    {
     
        // query to insert record
        $query = "UPDATE
                    " . $this->tbl_name . "
                  SET
                    name = :name
                    ,birthday = :birthday
                    ,document = :document
                    ,gender = :gender
                    ,address = :address
                  WHERE
                    id = :id";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->birthday=htmlspecialchars(strip_tags($this->birthday));
        $this->document=htmlspecialchars(strip_tags($this->document));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->address=htmlspecialchars(strip_tags($this->address));
        $this->id=htmlspecialchars(strip_tags($this->id));
     
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":birthday", $this->birthday);
        $stmt->bindParam(":document", $this->document);
        $stmt->bindParam(":gender", $this->gender);
        $stmt->bindParam(":address", $this->address);
        $stmt->bindParam(":id", $this->id);
        
        //var_dump($stmt);
     
        // execute query
        if($stmt->execute())
        {
            
            return true;
        }
     
        return false;
         
    }
    
    // update people
    function delete()
    {
     
        // query to insert record
        $query = "DELETE FROM
                    " . $this->tbl_name . "
                  WHERE
                    id = :id";
     
        // prepare query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
     
        // bind values
        $stmt->bindParam(":id", $this->id);
        
        //var_dump($stmt);
     
        // execute query
        if($stmt->execute())
        {
            
            return true;
        }
     
        return false;
         
    }
    
    // range of ages
    function readByAge()
    {
     
        // select all query
        $query = "select case when sum(b.faixa1) IS NULL THEN 0 ELSE sum(b.faixa1) END AS RANGE1, case when sum(b.faixa2) IS NULL THEN 0 ELSE sum(b.faixa2) END AS RANGE2, case when sum(b.faixa3) IS NULL THEN 0 ELSE sum(b.faixa3) END AS RANGE3, case when sum(b.faixa4) IS NULL THEN 0 ELSE sum(b.faixa4) END AS RANGE4, case when sum(b.faixa5) IS NULL THEN 0 ELSE sum(b.faixa5) END AS RANGE5 from (select case when a.years between 0 and 9 then count(a.years) end as faixa1 ,case when a.years between 10 and 19 then count(a.years) end as faixa2 ,case when a.years between 20 and 29 then count(a.years) end as faixa3 ,case when a.years between 30 and 39 then count(a.years) end as faixa4 ,case when a.years > 39 then count(a.years) end as faixa5  from (select floor(datediff(replace(cast(sysdate() as date),'-',''),date_format(str_to_date(birthday ,'%d/%m/%Y'), '%Y%m%d')) / 365) as years from people) as a group by a.years) as b";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
    // split by gender
    function readByGender()
    {
     
        // select all query
        $query = "select gender, count(*) as qtd  from people group by gender";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
     
        // execute query
        $stmt->execute();
     
        return $stmt;
    }
    
}